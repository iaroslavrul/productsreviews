# ProductsReviews

### Capabilities:

1. Adding a new review to an existing product;
1. Request for reviews on a specific product.
1. Elephantsql is used for cloud hosting Postgresql

### Installation:

1. After cloning / downloading the repository, execute dependent packages using the command
   `pip install -r requirements.txt`.
1. Then, to run the project from its root directory, you need to run the `python app.py {path to the file with products in the csv format} {path to the file with reviews in the csv format}` command.
   By default, `python app.py Products.csv Reviews.csv`
1. Local API testing can be done using the Insomnia REST client or the CURL console utility.

### Interaction via API:

1. To add a new review to an existing product, you need to run a PUT-query `http://127.0.0.1:5000/add/`. To add a new
   review to an existing product, you must run the command, and in the request body, specify the product
   identifier `asin`, to which you need to add a review, title `title` and review text `review`. For example,
   `{
      "asin": "B06X14Z8JP",
      "title": "test title",
      "review": "test review"
   }`
   ![](readme_img/1.png)
   If all data is entered correctly, then in response the request will return the original json.
1. To view all reviews for a specific product, you must run a GET-request `http://127.0.0.1:5000/get/<asin>/<page>`.
   Where `page` is an optional parameter, by default `page' = 1.
   
   For example, `http://127.0.0.1:5000/get/B06X14Z8JP/1`
   ![](readme_img/2.png)

   