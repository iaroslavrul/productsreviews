from sqlalchemy import PrimaryKeyConstraint, ForeignKey

from app import db


class Product(db.Model):
    """Class for interacting of Product data"""
    __tablename__ = 'products'
    __table_args__ = (
        PrimaryKeyConstraint('asin', 'title'),
    )

    asin = db.Column(db.String(20), unique=True)
    title = db.Column(db.String(150))

    def __init__(self, asin, title):
        self.asin = asin
        self.title = title

    def __repr__(self):
        return '<asin {}>'.format(self.asin)

    def serialize(self):
        return {
            'asin': self.asin,
            'title': self.title
        }


class Reviews(db.Model):
    """Class for interacting of Reviews data"""
    __tablename__ = 'reviews'
    __table_args__ = (
        PrimaryKeyConstraint('asin', 'title', 'review'),
    )
    asin = db.Column(db.String(20), ForeignKey('products.asin', ondelete='CASCADE'))
    title = db.Column(db.String(150))
    review = db.Column(db.String())

    def __init__(self, asin, title, review):
        self.asin = asin
        self.title = title
        self.review = review

    def __repr__(self):
        return '<asin {}>'.format(self.asin)

    def serialize(self, type_):
        if type_ == 'add':
            return {
                'asin': self.asin,
                'title': self.title,
                'review': self.review
            }
        else:
            return {
                'title': self.title,
                'review': self.review
            }
