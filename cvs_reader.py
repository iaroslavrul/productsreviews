import csv
import models
import app


def get_data_from_csv(products_csv, reviews_csv):
    """
    Reading data from csv files and writing them to the DB
    """
    with open(products_csv, 'r', encoding='utf-8') as file:
        products = csv.DictReader(file, delimiter=',')
        models.Product.query.delete()
        for product in products:
            new_product = models.Product(product['Asin'], product['Title'])
            app.db.session.add(new_product)
        try:
            app.db.session.commit()
        except Exception as e:
            print('Products add exception: ', e)
    with open(reviews_csv, 'r', encoding='utf-8') as file:
        reviews = csv.DictReader(file, delimiter=',')
        models.Reviews.query.delete()
        for review_ in reviews:
            new_review = models.Reviews(review_['Asin'], review_['Title'], review_['Review'])
            app.db.session.add(new_review)
        try:
            app.db.session.commit()
        except Exception as e:
            print('Reviews add exception: ', e)
