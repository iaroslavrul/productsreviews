from flask import Flask, jsonify, request, abort
from flask_sqlalchemy import SQLAlchemy
import configparser
import cvs_reader
import argparse
from flask_caching import Cache
import models

# argparse setups begin
parser = argparse.ArgumentParser(description='File paths: Products.csv, Reviews.csv')
parser.add_argument('products', nargs='?', const='Products.csv', type=str, help='Path to the Products.csv file',
                    default='Products.csv')
parser.add_argument('reviews', nargs='?', const='Reviews.csv', type=str, help='Path to the Reviews.csv file',
                    default='Reviews.csv')
args = parser.parse_args()
products_file_path = args.products
reviews_file_path = args.reviews
# argparse setups end

# config setups begin
config = configparser.ConfigParser()
config.read("settings.ini")
# config setups end

# Flask setups begin
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = config["Database"]["DB_URL"]
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CACHE_TYPE'] = 'simple'
# Flask setups end

# SQLAlchemy setups begin
db = SQLAlchemy(app)
# SQLAlchemy setups end

# Cache setups begin
cache = Cache(app)
# Cache setups end


@app.route('/add/', methods=['PUT'])
def add_review():
    """Function for adding a new comment to an existing product"""
    if not request.json:
        return jsonify(status='Required data not transferred'), 422
    asin = request.json['asin']
    title = request.json['title']
    review = request.json['review']
    new_review = models.Reviews(asin, title, review)
    db.session.add(new_review)
    try:
        db.session.commit()
    except Exception:
        return jsonify(status='This product is not in the DB'), 404
    return jsonify({'review': new_review.serialize('add')}), 201


@app.route("/get/<string:asin_>/", methods=['GET'])
@app.route("/get/<string:asin_>/<int:page>", methods=['GET'])
@cache.memoize(timeout=30)
def get_by_id(asin_, page=1):
    """Function to retrieve comments for a specific product"""
    n = int(config["Pagination"]["PRODUCTS_PER_PAGE"])
    result = []
    try:
        product = models.Product.query.filter_by(asin=asin_).first().serialize()
    except Exception:
        return jsonify(status='This product is not in the DB'), 404
    try:
        reviews = models.Reviews.query.filter_by(asin=asin_).all()
        result = [review.serialize('get') for review in reviews]
    except Exception:
        pass
    reviews_list = [result[i:i + n] for i in range(0, len(result), n)]
    available_pagination_range = len(reviews_list)
    if page < 1:
        page = 1
    if page > available_pagination_range:
        page = available_pagination_range
    if len(reviews_list) > 0:
        reviews_list = reviews_list[page - 1]
    else:
        reviews_list = []
    return {"asin": product['asin'], "title": product['title'], "review": reviews_list, "current_page": page,
            "available_pagination_range": available_pagination_range}


@app.errorhandler(405)
def method_not_allowed(e):
    """Handle 405 status code and return a JSON error"""
    return jsonify(status='Method not allowed', error=str(e)), 405


@app.errorhandler(400)
def bad_request(e):
    """Handle 400 status code and return a JSON error"""
    return jsonify(status='Bad Request', error=str(e)), 400


def main():
    cvs_reader.get_data_from_csv(products_file_path, reviews_file_path)
    app.run(port=5000)


if __name__ == '__main__':
    main()
